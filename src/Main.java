
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {

        Scanner in = new Scanner(System.in);
        String input = in.nextLine();
        System.out.println(calc(input));

    }

    //
    public static String calc(String input) throws Exception {
        String output = "0";
        String [] arithmeticExpression = input.split(" ");

        if (arithmeticExpression.length != 3) {
            throw new Exception();
        }

        Number num1 = conversionToNumber(arithmeticExpression[0]);
        Number num2 = conversionToNumber(arithmeticExpression[2]);
        if (num1.arabicOrRoman != num2.arabicOrRoman) {
            throw new Exception();
        }

        int result;
        switch (arithmeticExpression[1]) {
            case "+": result = num1.Arabic + num2.Arabic; break;
            case "-": result = num1.Arabic - num2.Arabic; break;
            case "*": result = num1.Arabic * num2.Arabic; break;
            case "/": result = num1.Arabic / num2.Arabic; break;
            default: throw new Exception();
        }

        if (num1.arabicOrRoman) {
            output = RomanNumeral.arabicToRoman(result);
        }
        else {
            output = String.valueOf(result);
        }

        return output;
    }

    // Эта функция проверяет, является ли строка арбаским или римским числом, входит ли число в диапазон от 1 до 10
    // И если все условия выполняются, возвращает всю необходимую информацию в виде объекта класса Number
    public static Number conversionToNumber(String number) throws Exception {
        Number num = new Number();
        String [] romanN1To10 = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};

        try {
            num.Arabic = Integer.parseInt(number);
            num.arabicOrRoman = false;
        } catch (Exception e) {
            int i = 0;
            boolean found = false;
            while (i < 10 && !found) {
                if (number.equals(romanN1To10[i])) {
                    num.Arabic = i+1;
                    num.Roman = number;
                    num.arabicOrRoman = true;
                    found = true;
                }
                i++;
            }

            if (!found) {
                throw new Exception();
            }
        }

        if (num.Arabic > 10 || num.Arabic < 1) {
            throw new Exception();
        }

        return num;
    }
}

// Этот класс был создан, чтобы функция conversionToNumber могла вернуть несколько значений
class Number {
    int Arabic;
    String Roman;
    boolean arabicOrRoman; //false - Arabic, true - Roman
}

//Этот enum был создан ради функции arabicToRoman, которая переводит арабское число в римское
enum RomanNumeral {
    C(100), LC(90), L(50), XL(40), X(10), IX(9), V(5), IV(4), I(1);

    private int arabic;
    private RomanNumeral(int n) {
        arabic = n;
    }

    //Эта функция учитывает, чтоб число было больше 0
    public static String arabicToRoman(int arabicN) throws Exception {

        if (arabicN < 1) {
            throw new Exception();
        }

        String romanN = "";
        for (RomanNumeral el:RomanNumeral.values()) {
            int amount = arabicN/el.arabic;
            for (int i = 0; i < amount; i++) {
                romanN += el.name();
                arabicN -= el.arabic;
            }

            if (arabicN <=0) {
                break;
            }
        }

        return romanN;
    }
}